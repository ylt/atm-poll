/**
 * Created by joe on 20/09/17.
 */

let rp = require('request-plus');
let request = rp();
let fs = require('fs');

async function run() {
    let postid = 71132;
    let res = await request({
        url: `https://community.monzo.com/posts/${postid}.json`,
        json: true
    });

    console.log(res);

    let poll = res.polls.poll;
    // "poll": {
    //     "options": [{
    //         "id": "2940bec74111b077588eb71e436bdf84",
    //         "html": "Option 1: 1% charge for ATM withdrawals in Europe, 2% charge for withdrawals Rest of World",
    //         "votes": 1649
    //     }, {
    //         "id": "b110921ce6c6e7dd9ae8123d17a5ca40",
    //         "html": "Option 2: 1.5% charge for ATM withdrawals everywhere outside the UK",
    //         "votes": 647
    //     }, {
    //         "id": "8f841d86765eed3faf80f399ebe38cd0",
    //         "html": "Option 3: £200 free allowance per month, 3% charge for withdrawals thereafter everywhere outside the UK",
    //         "votes": 3589
    //     }],
    //         "voters": 5885,
    //         "status": "open",
    //         "type": "regular",
    //         "public": "true",
    //         "name": "poll"
    // }
    for (let option of poll.options) {
        let numvotes = option.votes;
        option.voters = [];
        let stepsize = 50;
        for (let i = 0; (i * stepsize) < numvotes + 2; i++) {
            console.log(option.html, 'page '+i+' of '+(numvotes/stepsize));
            let voters = await request({
                url: `https://community.monzo.com/polls/voters.json?post_id=${postid}&poll_name=poll&option_id=${option.id}&offset=${i}&voter_limit=${stepsize}`,
                json: true
            });
            if (voters.poll[option.id] != null)
                option.voters = option.voters.concat(voters.poll[option.id]);
        }
    }
    fs.writeFile('voters.json', JSON.stringify(poll, null, ' '));
}

run();