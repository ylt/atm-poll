/**
 * Created by joe on 27/09/17.
 */

let fs = require('fs');
const _ = require('lodash');
let request = require('request-promise');
let moment = require('moment');

async function run() {
    sample = _.map(fs.readdirSync('voters'), s => s.substring(0,s.length-5));

    for (let username of sample) {
        let profile = JSON.parse(fs.readFileSync(`voters/${username}.json`));
        if (!profile.summary) {
            console.log(`doing ${username}`);

            let summary = await request({url: `https://community.monzo.com/u/${username}/summary.json`, json: true});
            profile.summary = summary;

            fs.writeFileSync(`voters/${username}.json`, JSON.stringify(profile, null, ' '));
        }

    }
}

run();