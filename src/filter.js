/**
 * Created by joe on 25/09/17.
 */
let fs = require('fs');
const _ = require('lodash');
let request = require('request-promise');
let moment = require('moment');

async function run() {

    let data = JSON.parse(fs.readFileSync('voters.json'));
    let voters = _.flatMap(data.options, o => o.voters);
    console.log(_.uniq(_.map(voters, v => v.title)))

    let step = 100;
    for (let i = 0; i < 23722; i += step) {
        let options = [];
        let last = [];
        for (let option of data.options) {
            let voters = _.filter(option.voters, voter => {
                return voter.id >= i && voter.id < i + step;
            });

            if (voters.length > 0)
                last.push(_.maxBy(voters, v => v.id))

            options.push(voters.length);
            // option.voters = voters;

            // console.log(voters.length, option.html);

        }

        let username = _.maxBy(last, v => v.id).username;

        let profile = await request({url: `https://community.monzo.com/u/${username}.json`, json: true});
        let date = moment(profile.user.created_at).format('DD/MM/YYYY HH:mm:ss');

        console.log(`${date}, ${options[0]}, ${options[1]}, ${options[2]}`)
        // console.log(i, options, date);
    }
}
run();

// console.log(data);