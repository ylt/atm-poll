/**
 * Created by joe on 27/09/17.
 */

let fs = require('fs');
const _ = require('lodash');
let request = require('request-promise');
let moment = require('moment');

function rel2duration(rel) {
    //https://github.com/discourse/discourse/blob/master/config/locales/server.en.yml#L554
    //http://www.rubydoc.info/github/discourse/discourse/FreedomPatches%2FRails4.distance_of_time_in_words
    let match = null;
    if (match = /^(< )?([0-9]+)s$/.exec(rel)) {
        return moment.duration(parseInt(match[2]), 'seconds');
    }
    else if (match = /^(< )?([0-9]+)m$/.exec(rel)) {
        return moment.duration(parseInt(match[2]), 'minutes');
    }
    else if (match = /^(< )?([0-9]+)h$/.exec(rel)) {
        return moment.duration(parseInt(match[2]), 'hours');
    }
    else if (match = /^(< )?([0-9]+)d$/.exec(rel)) {
        return moment.duration(parseInt(match[2]), 'days');
    }
    else {
        console.log('not found', rel);
    }
}

async function run() {

    let data = JSON.parse(fs.readFileSync('voters.json'));
    // let voters = _.flatMap(data.options, o => o.voters);

    let sample = [];

    sample = _.map(fs.readdirSync('voters'), s => s.substring(0,s.length-5));
    //
    // for (let option of data.options) {
    //     let voters = _.filter(option.voters, v => sample.indexOf(v.username) !== -1);
    //
    //     console.log(`${voters.length} / ${option.voters.length}`,
    //         '('+((voters.length / option.voters.length)*100)+'%)', option.html);
    // }

    // for (let username of sample) {
    //     let profile = JSON.parse(fs.readFileSync(`voters/${username}.json`));
    //
    //     let duration = rel2duration(profile.summary.user_summary.time_read);
    //
    //     console.log(duration.asMinutes())
    //
    // }


    function getVotes(filter) {
        let votes = [];
        for (let option of data.options) {
            let voters = _.filter(option.voters, v => {
                if (sample.indexOf(v.username) === -1) return false;

                let profile = JSON.parse(fs.readFileSync(`voters/${v.username}.json`));

                let duration = rel2duration(profile.summary.user_summary.time_read);

                return filter(profile, duration);
            });

            votes.push(voters.length);
        }
        return votes;
    }

    let values = [];
    for (let username of sample) {
        let profile = JSON.parse(fs.readFileSync(`voters/${username}.json`));
        let duration = rel2duration(profile.summary.user_summary.time_read);
        values.push(duration.asMinutes())
    }
    let peak = _.max(values);

    console.log(peak);

    let step = 1;
    for (let i = 0; i < peak; i+= step) {


        if (i > 10)
            step = 10000;
        else if (i > 40)
            step = 60;
        else if (i > 10)
            step = 5;

        let votes = getVotes((p,d) => {
            return p.summary.user_summary.post_count >= i && p.summary.user_summary.post_count < i + step
        })

        console.log(_.concat([i], votes).join(', '));

    }


}

run();