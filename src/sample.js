/**
 * Created by joe on 27/09/17.
 */

let fs = require('fs');
const _ = require('lodash');
let request = require('request-promise');
let moment = require('moment');

async function run() {

    let data = JSON.parse(fs.readFileSync('voters.json'));
    let voters = _.flatMap(data.options, o => o.voters);

    let sample = [];

    sample = _.map(fs.readdirSync('voters'), s => s.substring(0,s.length-5));

    while (sample.length <= 598) {
        let user = _.sample(voters);

        if (sample.indexOf(user) !== -1)
            continue;

        sample.push(user.username);

        let profile = await request({url: `https://community.monzo.com/u/${user.username}.json`, json: true});
        fs.writeFileSync(`voters/${user.username}.json`, JSON.stringify(profile, null, ' '));
    }
}

run();